/** Aplikacja skanująca playlistę i sprawdzająca czy wszystkie napisy na dany dzień są dostępne w Subtitle Serwerze **/

import java.awt.*;
import java.io.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
public class ukladaczExtraHD extends JFrame {
        private static final long serialVersionUID = 1L;

        JProgressBar current;
        JTextArea out;
        JButton find;
        Thread runner;
        int num = 0;
        GridBagConstraints c;
        Container contentPane;

        public ukladaczExtraHD() {
            super("NAPISOINATOR EXYUGO");

            JButton buttonL = new JButton("SEARCH");
            final JTextArea jt = new JTextArea();
            jt.setEditable(false);
            JScrollPane listScrollPane2 = new JScrollPane(jt);

            current = new JProgressBar(0, 10);
            current.setValue(0);
            current.setStringPainted(true);

            contentPane = getContentPane();
            contentPane.setLayout((new GridBagLayout()));
            c = new GridBagConstraints();

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.5;
            c.gridx = 0;
            c.gridy = 3;
            contentPane.add(buttonL, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.ipady = 150;
            c.weightx = 0.0;
            c.gridwidth = 3;
            c.gridx = 0;
            c.gridy = 5;
            contentPane.add(listScrollPane2, c);

            buttonL.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent arg0) {

                    openFile(jt);
                }
            });
        }

        public void openFile(JTextArea jt) {
            JFileChooser chooser = new JFileChooser();
            // chooser.setCurrentDirectory(new File("C:\\SUBS"));
            chooser.setCurrentDirectory(new File("V:\\PLAYLIST"));

            chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().endsWith(".csv") || f.getName().endsWith(".ply");
                }

                public String getDescription() {
                    return "CSV/PLY files";
                }
            });
            int r = chooser.showOpenDialog(this);
            if (r != JFileChooser.APPROVE_OPTION)
                return;
//            File f = chooser.getSelectedFile();
            File f =  chooser.getCurrentDirectory();
//            jt.append("PLAYLIST: " + chooser.getSelectedFile().getName() + "\n" + "\n");
            loguj(f, jt);
//            jt.append("END" + "\n" + "\n");
        }

        public void loguj(File ff, JTextArea jt) {
            try {
                // language sequences
                final String SRB = "4 0 0 0 26 36 0 0";
                final String CRO = "4 0 0 0 26 4 0 0";
                final String ALB = "4 0 0 0 28 4 0 0";
                final String MAC = "4 0 0 0 47 4 0 0";
                final String ROM = "4 0 0 0 24 4 0 0";
                final String BUL = "4 0 0 0 2 4 0 0";

                // checking sub files
                File folder = new File("Z:\\");
                File[] listOfFiles = folder.listFiles();
                ArrayList sublist = new ArrayList();

                for (File file22 : listOfFiles) {
                    if (file22.isFile()) {
                        sublist.add(file22.getName());
                    }
                }

                String[][] tablica = new String[sublist.size()][2];

                for (int i = 0; i < sublist.size(); i++) {
                    // System.out.println("FILENAME: " + sublist.get(i));

                    BufferedReader reader = null;
                    reader = new BufferedReader(new FileReader("Z:\\" + sublist.get(i)));
                    // reader = new BufferedReader(new FileReader("C:\\SUBS\\SUBS\\"
                    // + sublist.get(i)));

                    String stringtoanalyze = "";
                    String s2;
                    int timer = 4;
                    while ((s2 = reader.readLine()) != null && timer > 1) {
                        stringtoanalyze = stringtoanalyze + s2;
                        timer--;
                    }

                    String asci = "";

                    if (stringtoanalyze.length() > 300)
                        stringtoanalyze = stringtoanalyze.substring(0, 300);

                    int a = stringtoanalyze.indexOf("EXY");
                    System.out.println(a);
                    if (a >= 0) {
                        tablica[i][0] = stringtoanalyze.substring(a, a + 12);

                        if (stringtoanalyze.substring(a + 12, a + 13).equals(" ")) {
                            jt.append("SPACJA PROBLEM: " + sublist.get(i) + "\n");
                        }

                        for (int j = 0; j < stringtoanalyze.length(); ++j) {
                            char c = stringtoanalyze.charAt(j);
                            int k = (int) c;

                            asci = asci + k + " ";
                            tablica[i][1] = asci;
                        }

                    } else {
                        tablica[i][0] = "";
                        tablica[i][1] = "";

                    }
                }

                for (int i = 0; i < tablica.length; i++)
                    for (int j = 0; j < tablica[i].length; j++) {
                        if (tablica[i][0] == null)
                            jt.append("NULL PROBLEM" + "\n");
                    }
                File folderOfFiles = ff;
                File[] listOfPly = folderOfFiles.listFiles();
                Arrays.sort(listOfPly);

                for (File plyFile : listOfPly) {
                    jt.append("PLAYLIST: " + plyFile.getName() + "\n" + "\n");
                    // czytanie pliku
                    if(!plyFile.isDirectory()) {
                        FileReader fr = new FileReader(plyFile);
                        BufferedReader br = new BufferedReader(fr);
                        String s;
                        if (plyFile.getAbsolutePath().endsWith(".ply")) {
                            while ((s = br.readLine()) != null) {
                                String[] a = null;

                                s = s.replaceAll("\\\"", "");
                                if (s.charAt(1) == ':') {
                                    a = s.split(";");

                                    String mediaid = ((a[0].split("\\\\"))[2]).substring(0, (a[0].split("\\\\"))[2].length() - 4);
                                    System.out.println(mediaid);

                                    int srbcount = 0;
                                    int crocount = 0;
                                    int albcount = 0;
                                    int maccount = 0;
                                    int romcount = 0;
                                    int bulcount = 0;
                                    String warningSrb = "";
                                    String warningCro = "";
                                    String warningAlb = "";
                                    String warningMac = "";
                                    String warningRom = "";
                                    String warningBul = "";

                                    if (mediaid.startsWith("EXYUGOHD0")) {
                                        for (int i = 0; i < tablica.length; i++) {
                                            if(tablica[i][0].equals(mediaid)){
                                                switch (tablica[i][1]) {
                                                    case SRB:{
                                                        srbcount++;
                                                        warningSrb += sublist.get(i) + "\n";
                                                        if(srbcount > 1){
                                                            jt.append(warningSrb);
                                                        }
                                                    }
                                                    case CRO:{
                                                        crocount++;
                                                        warningCro += sublist.get(i) + "\n";
                                                        if(crocount > 1){
                                                            jt.append(warningCro);
                                                        }
                                                    }
                                                    case ALB:{
                                                        albcount++;
                                                        warningAlb += sublist.get(i) + "\n";
                                                        if(albcount > 1){
                                                            jt.append(warningAlb);
                                                        }
                                                    }
                                                    case MAC:{
                                                        maccount++;
                                                        warningMac += sublist.get(i) + "\n";
                                                        if(maccount > 1){
                                                            jt.append(warningMac);
                                                        }
                                                    }
                                                    case ROM:{
                                                        romcount++;
                                                        warningRom += sublist.get(i) + "\n";
                                                        if(romcount > 1){
                                                            jt.append(warningRom);
                                                        }
                                                    }
                                                    case BUL: {
                                                        bulcount++;
                                                        warningBul += sublist.get(i) + "\n";
                                                        if(bulcount > 1){
                                                            jt.append(warningBul);
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                        if (srbcount == 0) {
                                            jt.append(mediaid + " MISSING SRB" + "\n");
                                        } else if (srbcount > 1) {
                                            jt.append(mediaid + " MULTIPLE SRB" + "\n");
                                        }

                                        if (crocount == 0) {
                                            jt.append(mediaid + " MISSING CRO" + "\n");
                                        } else if (crocount > 1) {
                                            jt.append(mediaid + " MULTIPLE CRO" + "\n");
                                        }

                                        if (albcount == 0) {
                                            jt.append(mediaid + " MISSING ALB" + "\n");
                                        } else if (albcount > 1) {
                                            jt.append(mediaid + " MULTIPLE ALB" + "\n");
                                        }

                                        if (maccount == 0) {
                                            jt.append(mediaid + " MISSING MAC" + "\n");
                                        } else if (maccount > 1) {
                                            jt.append(mediaid + " MULTIPLE MAC" + "\n");
                                        }
//                                        unused right now
//                                      if (romcount == 0) {
//                                          jt.append(mediaid + " MISSING ROM" + "\n");
//                                      } else if (romcount > 1) {
//                                          jt.append(mediaid + " MULTIPLE ROM" + "\n");
//                                      }

                                        if (bulcount == 0) {
                                            jt.append(mediaid + " MISSING BUL" + "\n");
                                        } else if (bulcount > 1) {
                                            jt.append(mediaid + " MULTIPLE BUL" + "\n");
                                        }
                                    }
                                }
                            }
                        }

                    jt.append("END" + "\n" + "\n");
                        fr.close();
                    }
                }



            } catch (Exception e) {
                System.out.println("PROBLEM");
                jt.append("PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM PROBLEM");
                e.printStackTrace();
            }
        }

        public static void main(String s[]) {
            JFrame frame = new ukladaczExtraHD();
            frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            frame.setSize(600, 235);

        }
}